console.log( 'I am again!!' );

const arr = [ 1, 2, 3 ];
const iAmJavascriptES6 = () => console.log( ...arr );
window.iAmJavascriptES6 = iAmJavascriptES6;

import style from "./_scss/main.scss";