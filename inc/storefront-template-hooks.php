<?php
/**
 * Storefront WooCommerce hooks
 *
 * @package storefront
 */


/**
 * Header
 *
 * @see storefront_product_search()
 * @see storefront_header_cart()
 */

add_action( 'after_setup_theme', 'storefront_child_lg_header_hooks' );
function storefront_child_lg_header_hooks() {
    remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
    add_action( 'storefront_header', 'storefront_secondary_navigation', 15 );
}
